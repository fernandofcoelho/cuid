# CUID

CLI para criação de CUIDs em arquivo .csv

## Instalação

Download: [linux](https://bitbucket.org/fernandofcoelho/cuid/downloads/cuid) / [windows](https://bitbucket.org/fernandofcoelho/cuid/downloads/cuid.exe)

## Uso

Terminal:  
`$ cd path/to/cuid`  
`$ ./cuid <qtde>`  

#### Windows

Executar terminal como Administrador
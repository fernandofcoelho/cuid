package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/lucsky/cuid"
)

var DataLogger *log.Logger

func init() {
	file, err := os.OpenFile("cuid.csv", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	DataLogger = log.New(file, "", 0)
}

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		displayHelp()
		return
	}
	amount, err := strconv.Atoi(args[0])
	if err != nil {
		displayHelp()
	} else {
		for i := 0; i < amount; i++ {
			DataLogger.Printf("%s,", cuid.New())
		}
		fmt.Printf("cuid.csv atualizado com %d CUIDs\n", amount)
	}
}

func displayHelp() {
	fmt.Printf("Uso: ./cuid <qtde>\nExemplo: ./cuid 123\n")
}
